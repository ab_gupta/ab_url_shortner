package tech.abgupta.urlshortnerspringbootpsql.test;

import org.junit.Ignore;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import tech.abgupta.urlshortnerspringbootpsql.dto.UrlResponse;
import tech.abgupta.urlshortnerspringbootpsql.entity.Url;
import tech.abgupta.urlshortnerspringbootpsql.repository.UrlRepository;
import tech.abgupta.urlshortnerspringbootpsql.service.UrlService;
import tech.abgupta.urlshortnerspringbootpsql.test.controller.UrlControllerTest;
import tech.abgupta.urlshortnerspringbootpsql.test.service.UrlServiceTest;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

//@RunWith(SpringRunner.class)
@SpringBootTest
@RunWith(Suite.class)
@Suite.SuiteClasses({ UrlServiceTest.class, UrlControllerTest.class })
public class UrlShortnerSpringbootPsqlApplicationTests {
    @Test
    void contextLoads() {

    }

}
