package tech.abgupta.urlshortnerspringbootpsql.test.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import tech.abgupta.urlshortnerspringbootpsql.controller.UrlController;
import tech.abgupta.urlshortnerspringbootpsql.entity.Url;
import tech.abgupta.urlshortnerspringbootpsql.service.UrlService;

import java.util.Date;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(value = UrlController.class)
public class UrlControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UrlService urlService;


    Url mockUrl = new Url(1,"www.google.com",new Date());

//    @Before
//    public void setup(){
//        this.mockUrl = new Url(1,"www.google.com",new Date());
//    }

//    @Test
//	void contextLoads() {
//	}

    @Test
    public void getAndRedirectTest () throws Exception {
        System.out.println("getandredirectTest");
        Mockito.when( urlService.getOriginalUrl("aaaa")).thenReturn("https://www.google.com");

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .get("/api/getlong/{shortUrl}", "aaaa");

        MvcResult result = mockMvc.perform(requestBuilder)
                                    .andExpect(status().is3xxRedirection())
                                    .andReturn();

        MockHttpServletResponse response = result.getResponse();

        Optional<String> opt=  Optional.ofNullable(response.getRedirectedUrl());

        String redirectedUrl = opt.orElse("not found");

        assertEquals("https://www.google.com", redirectedUrl);
    }

    @Test
    public void convertToShortUrlTest() throws Exception {
        System.out.println("convertTOShortUrlTest");
        Mockito.when(urlService.convertToShortUrl("https://www.google.com")).thenReturn("aaaa");

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/create-short/")
                .content("https://www.google.com")
                .contentType(MediaType.TEXT_PLAIN_VALUE)
                .characterEncoding("utf-8");

        MvcResult result = mockMvc.perform(requestBuilder)
                                    .andExpect(status().isOk())
                                    .andReturn();

        MockHttpServletResponse response = result.getResponse();

        Optional<String> opt = Optional.ofNullable(response.getContentAsString());

        String shortUrl = (String) opt.orElse("not Found");

        assertEquals(shortUrl,"aaaa");

    }
}
