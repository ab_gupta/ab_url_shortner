package tech.abgupta.urlshortnerspringbootpsql.test.service;

import org.junit.Rule;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import tech.abgupta.urlshortnerspringbootpsql.dto.UrlResponse;
import tech.abgupta.urlshortnerspringbootpsql.entity.Url;
import tech.abgupta.urlshortnerspringbootpsql.repository.UrlRepository;
import tech.abgupta.urlshortnerspringbootpsql.service.UrlService;

import javax.persistence.EntityNotFoundException;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

@RunWith(SpringRunner.class)
@ExtendWith(MockitoExtension.class)
@SpringBootTest()
public class UrlServiceTest {
    @Autowired
    private UrlService urlService;

    // MockBean is the annotation provided by Spring that wraps mockito one
    // Annotation that can be used to add mocks to a Spring ApplicationContext.
    // If any existing single bean of the same type defined in the context will be replaced by the mock,
    @MockBean
    private UrlRepository urlRepository;

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Test
    public void showTest() throws ExecutionException, InterruptedException {
        System.out.println("showTest");
        when(urlRepository.findAll()).thenReturn(Stream.
                of(new Url(1, "www.google.com", new Date()),
                        new Url(2, "www.youtube.com", new Date()),
                        new Url(3, "www.facebok.com", new Date())).collect(Collectors.toList()));

        CompletableFuture<List<UrlResponse>> result =  urlService.showasync();
        assertEquals(3, result.get().size());
    }

    @Test
    public void removeExpiredTest(){
        System.out.println("removeExpiredTest");
        Date currentDate = new Date();
        urlRepository.removeAllExpired(currentDate);
        verify(urlRepository, times(1)).removeAllExpired(currentDate);
    }

    @Test
    public void getOriginalUrlTest(){
        System.out.println("getOriginalUrlTest");
        long id = 1;
        long MIN_IN_MS = 1000*60;
        Date currentDate = new Date(System.currentTimeMillis() + (5 * MIN_IN_MS));
        String longUrl = "https://www.google.com";
        Url url = new Url(id, longUrl, currentDate);
        when(urlRepository.findById(id)).thenReturn(java.util.Optional.of(url));
        String result = urlService.getOriginalUrl("aaab");

        assertEquals(longUrl, result);
        verify(urlRepository, times(1)).findById(id);
        verify(urlRepository, times(0)).delete(url);
    }

    @Test
    public void getOriginalUrl2Test(){
        System.out.println("getOriginalUrl2Test");
        long id = 1;
        long MIN_IN_MS = 1000*60;
        Date currentDate = new Date(System.currentTimeMillis() - (20 * MIN_IN_MS));
        Url url = new Url(id, "www.google.com", currentDate);
        when(urlRepository.findById(id)).thenReturn(java.util.Optional.of(url));
        exception.expect(EntityNotFoundException.class);
        String result = urlService.getOriginalUrl("aaab");
//        assertEquals("Link Expired!", result);
        verify(urlRepository, times(1)).findById(id);
        verify(urlRepository, times(1)).delete(url);
    }

    @Test
    public void convertToShortUrlTest(){
        System.out.println("convertToShortUrlTest");
        String longUrl = "https://www.google.com";
        long id=1;
        long MIN_IN_MS = 1000*60;
        Date currentDate = new Date(System.currentTimeMillis() + (20 * MIN_IN_MS));
        Url url = new Url(id, longUrl, currentDate);
        when(urlRepository.findByLongUrl(longUrl)).thenReturn(url);
        String response = urlService.convertToShortUrl(longUrl);
        assertEquals(4,response.length());
        assertEquals("aaab", response);
        verify(urlRepository, times(1)).findByLongUrl(any());
        verify(urlRepository, times(1)).updateExpiry(any(Date.class), any(long.class));
    }

    @Test
    public  void convertToShortUrlBestCaseTest(){
        System.out.println("convertToShortUrlBestCaseTest");
        String longUrl = "https://www.google.com";
        long id=1, count=1;
        long MIN_IN_MS = 1000*60;
        Date currentDate = new Date(System.currentTimeMillis() + (20 * MIN_IN_MS));
        Url url = new Url(id, longUrl, currentDate);
        when(urlRepository.findByLongUrl(longUrl)).thenReturn(null);
        when(urlRepository.count()).thenReturn(count);
        when(urlRepository.save(any())).thenReturn(url);
        String response = urlService.convertToShortUrl(longUrl);
        assertEquals("aaab", response);
        verify(urlRepository, times(1)).findByLongUrl(any(String.class));
        verify(urlRepository, times(1)).count();
        verify(urlRepository, times(1)).save(any());
    }
}
