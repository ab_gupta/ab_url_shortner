package tech.abgupta.urlshortnerspringbootpsql.entity;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

//@Data
//@NoArgsConstructor
//@AllArgsConstructor
//@Builder
//@ToString
//@EqualsAndHashCode
@Entity
@Table(name = "urlmap")
public class Url {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "longUrl", nullable = false)
    private String longUrl;

    @Column(nullable = false)
    private Date expiryDate;

    public  Url(){}

    public Url(long id, String longUrl, Date date){
        this.id = id;
        this.longUrl = longUrl;
        this.expiryDate = date;
    }
    public long getId() {
        return id;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLongUrl() {
        return longUrl;
    }

    public void setLongUrl(String longUrl) {
        this.longUrl = longUrl;
    }

}
