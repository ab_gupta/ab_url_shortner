package tech.abgupta.urlshortnerspringbootpsql;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication
public class UrlShortnerSpringbootPsqlApplication {

	@Value("${homeMsg}")
	private String homeMsg;
	@RequestMapping("/")
	String home() {
		return homeMsg;
	}

	public static void main(String[] args) {
		SpringApplication.run(UrlShortnerSpringbootPsqlApplication.class, args);
	}

}
