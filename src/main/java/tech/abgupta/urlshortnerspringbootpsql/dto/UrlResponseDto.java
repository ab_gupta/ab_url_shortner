package tech.abgupta.urlshortnerspringbootpsql.dto;

import java.util.Date;
import java.util.List;

public class UrlResponseDto {
    private  List<UrlResponse> urlResponseList;
    public UrlResponseDto(List<UrlResponse> urlResponses){
        this.urlResponseList = urlResponses;
    }
}
