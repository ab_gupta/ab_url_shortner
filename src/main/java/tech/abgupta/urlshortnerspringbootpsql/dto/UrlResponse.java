package tech.abgupta.urlshortnerspringbootpsql.dto;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import tech.abgupta.urlshortnerspringbootpsql.entity.Url;
import tech.abgupta.urlshortnerspringbootpsql.service.BaseConversion;

import java.util.Date;

@Data
public class UrlResponse {

    private String longUrl, shorturl;
    Date expiryDate;
    long id;
    public UrlResponse(Url url, String shortUrl){
        this.id = url.getId();
        this.longUrl = url.getLongUrl();
        this.shorturl = shortUrl;
        this.expiryDate = url.getExpiryDate();
    }
}
