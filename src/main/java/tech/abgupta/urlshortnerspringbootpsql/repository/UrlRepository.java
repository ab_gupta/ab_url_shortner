package tech.abgupta.urlshortnerspringbootpsql.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import tech.abgupta.urlshortnerspringbootpsql.entity.Url;

import javax.xml.crypto.Data;
import java.util.Date;
import java.util.List;

@Repository
public interface UrlRepository extends JpaRepository<Url, Long> {

    Url findByLongUrl (String longUrl);
    List<Url> findAll();

    @Modifying
    @Query(value = "delete from urlmap where expiry_date = (select min(expiry_date) from urlmap)", nativeQuery = true)
    void deleteOldestUrl();

    @Modifying
    @Query(value = "update urlmap set expiry_date = :new_expiry_date where id = :id", nativeQuery = true)
    void updateExpiry(Date new_expiry_date, long id);

    @Modifying
    @Query(value = "delete from urlmap where expiry_date <= :current_date", nativeQuery = true)
    void removeAllExpired(Date current_date);

}
