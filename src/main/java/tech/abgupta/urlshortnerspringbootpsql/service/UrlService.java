package tech.abgupta.urlshortnerspringbootpsql.service;

import org.apache.commons.validator.routines.UrlValidator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import tech.abgupta.urlshortnerspringbootpsql.dto.UrlLongRequest;
import tech.abgupta.urlshortnerspringbootpsql.dto.UrlResponse;
import tech.abgupta.urlshortnerspringbootpsql.dto.UrlResponseDto;
import tech.abgupta.urlshortnerspringbootpsql.entity.Url;
import tech.abgupta.urlshortnerspringbootpsql.exception.BadUrlException;
import tech.abgupta.urlshortnerspringbootpsql.repository.UrlRepository;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//since service is executing custom update/delete operation using @Transactional here and @modifying there in repository method which its calling
@Transactional
@Service
@Component
public class UrlService {
//    @Autowired
//    private UrlRepository urlRepository;
    private final UrlRepository urlRepository;
    private final BaseConversion conversion;

    @Value("${elevate}")
    private long elevate ;

    @Value("${maxEntries}")
    private long maxEntries;

    @Value("${ttlDays}")
    private int ttldays;

    public UrlService(UrlRepository urlRepository, BaseConversion baseConversion) {
        this.urlRepository = urlRepository;
        this.conversion = baseConversion;
//        this.elevate = 242235;
//        this.maxEntries = 10;
    }

    public long findBase(){
        long got = conversion.decode("aG5l");
        System.out.println(got + conversion.encode(got));
        return got;
    }

    Date getExpiryDate(int ttl){
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DAY_OF_MONTH, ttl);
        return cal.getTime();
//        long MIN_IN_MS = 1000*60;
//        return new Date(System.currentTimeMillis() + (ttl * MIN_IN_MS));
    }

    public Boolean validateUrl(String url){
        UrlValidator urlValidator = new UrlValidator();
        return urlValidator.isValid(url);
    }

    public String convertToShortUrl(String longUrl) {

        if(validateUrl(longUrl) == false){
            throw new BadUrlException(longUrl);
        }

        Url isExists = urlRepository.findByLongUrl(longUrl);
        if(isExists != null){
            urlRepository.updateExpiry(getExpiryDate(ttldays), isExists.getId());
            return  conversion.encode(isExists.getId()+elevate);
        }

        Url url = new Url();
        url.setLongUrl(longUrl);
        url.setExpiryDate(getExpiryDate(ttldays));

        if(urlRepository.count() == maxEntries){
            urlRepository.deleteOldestUrl();
        }

        Url entity = urlRepository.save(url);

        long shortUrlCode = elevate + entity.getId();
        return conversion.encode(shortUrlCode);
    }

    public String getOriginalUrl(String shortUrl) {
        long id = conversion.decode(shortUrl) - elevate;

        Url entity = urlRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("There is no url mapped with " + shortUrl));

        if (entity.getExpiryDate() != null && entity.getExpiryDate().before(new Date())){
            urlRepository.delete(entity);
            throw new EntityNotFoundException(shortUrl +" Link expired!");
        }
        return entity.getLongUrl();
    }

    public HttpStatus removeExpired(){
        Date date = new Date();
        urlRepository.removeAllExpired(date);
        return HttpStatus.OK;
    }

    @Async
    public CompletableFuture<List<UrlResponse>> showasync(){
        System.out.println("Request to get a list of urls : " + Thread.currentThread().getName());
        List<Url> urlList = urlRepository.findAll();
        List<UrlResponse> responses = new ArrayList<>();
        for(Url u : urlList){
            responses.add(new UrlResponse(u, conversion.encode(u.getId()+elevate)));
         }
        return CompletableFuture.completedFuture(responses);
    }
}
