package tech.abgupta.urlshortnerspringbootpsql.service;

import org.apache.commons.validator.routines.UrlValidator;
import org.springframework.stereotype.Service;

@Service
public class BaseConversion {
    private static final String allowedString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    private char[] allowedCharacters = allowedString.toCharArray();
    private int base = allowedCharacters.length;

    public String encode(long input){
        StringBuilder encodedString = new StringBuilder();

        while (input > 0){
            long rem = input % base;
            if(rem==0){
                encodedString.append(allowedCharacters[base-1]);
                input = input/base - 1;
            }
            else{
                encodedString.append(allowedCharacters[(int)rem-1]);
                input = input/base;
            }
        }

        return encodedString.reverse().toString();
    }

    public long decode(String input) {
        char[] characters = input.toCharArray();
        long length = characters.length;

        long decoded = 0;

        for(int i=0;i<length;i++){
            decoded = decoded * base;
            decoded = decoded + allowedString.indexOf(characters[i])+1;
        }

        return decoded;
    }

    public String sanitizeURL(String url) {
        if (url.substring(0, 7).equals("http://"))
            url = url.substring(7);

        if (url.substring(0, 8).equals("https://"))
            url = url.substring(8);

        if (url.charAt(url.length() - 1) == '/')
            url = url.substring(0, url.length() - 1);
        return url;
    }
}
