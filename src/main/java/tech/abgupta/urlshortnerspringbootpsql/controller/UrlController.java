package tech.abgupta.urlshortnerspringbootpsql.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.abgupta.urlshortnerspringbootpsql.dto.UrlLongRequest;
import tech.abgupta.urlshortnerspringbootpsql.service.UrlService;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.net.URI;

@RestController
@RequestMapping("/api")
public class UrlController {
    @Autowired
    UrlService urlService;

    @PostMapping("/create-short")
    public ResponseEntity<?> convertToShortUrl(@RequestBody String longUrl)  {
        String shortUrl = urlService.convertToShortUrl(longUrl);
        if(shortUrl == null) return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Please enter a valid url");
        else return ResponseEntity.status(HttpStatus.OK).body(shortUrl);
    }

    @GetMapping("/getlong/{shortUrl}")
    @Cacheable(value = "urls", key = "#shortUrl", sync = true)
    public ResponseEntity<?> getAndRedirect(@PathVariable String shortUrl) {
        String originalUrl = urlService.getOriginalUrl(shortUrl);
        return ResponseEntity.status(HttpStatus.FOUND)
                .location(URI.create(originalUrl))
                .build();
    }
}
