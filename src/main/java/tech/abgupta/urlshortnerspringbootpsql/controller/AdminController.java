package tech.abgupta.urlshortnerspringbootpsql.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.abgupta.urlshortnerspringbootpsql.dto.UrlResponse;
import tech.abgupta.urlshortnerspringbootpsql.dto.UrlResponseDto;
import tech.abgupta.urlshortnerspringbootpsql.entity.Url;
import tech.abgupta.urlshortnerspringbootpsql.service.UrlService;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.function.Function;

@RestController
@RequestMapping("/admin/")
public class AdminController {
    private final UrlService urlService;

    public AdminController(UrlService urlService) {
        this.urlService = urlService;
    }

    @GetMapping("clear")
    public HttpStatus deleteExpired(){
        return urlService.removeExpired();
    }


    @GetMapping("showall")
    public @ResponseBody CompletableFuture<ResponseEntity> showAll(){
        return urlService.showasync().<ResponseEntity>thenApply(ResponseEntity::ok)
                                                .exceptionally(handleShowallException);
    }
    private static Function<Throwable, ResponseEntity<? extends List<UrlResponse>>> handleShowallException = throwable -> {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    };

}
